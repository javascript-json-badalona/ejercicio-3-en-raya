/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-new */
class App {

  constructor() {
    this.jugadorActual = null;
    this.partidaFinalizada = false;
    this.fichas = {
      1: 'X',
      2: 'O'
    };

    this.asignaJugadorInicial();
    this.cargaElementosHTML();
    this.anyadeListeners();
    this.actualizaInfo();
  }

  anyadeListeners() {
    for (let casilla of this.casillas) {
      casilla.addEventListener('click', () => {
        if (!this.compruebaCasillaVacia(casilla)) {
          return false;
        }
        this.colocarFicha(casilla);
      });
    }
  }

  colocarFicha(casilla) {
    if (this.partidaFinalizada) {
      return false;
    }
    casilla.dataset.jugador = this.jugadorActual;
    if (this.check3EnRaya()) {
      this.finalizarPartida();
    } else {
      this.cambiaTurno();
    }
  }

  finalizarPartida() {
    this.partidaFinalizada = true;
    this.mensaje.textContent = 'Ha ganado el jugador:';
    this.tablero.classList.add('off');
  }

  cambiaTurno() {
    if (++this.jugadorActual > 2) {
      this.jugadorActual = 1;
    }
    this.actualizaInfo();
  }

  cargaElementosHTML() {
    this.mensaje = document.querySelector('.info .mensaje');
    this.turno = document.querySelector('.info .turno');
    this.casillas = document.querySelectorAll('.casilla');
    this.tablero = document.querySelector('.tablero');
  }

  compruebaCasillaVacia(casilla) {
    return casilla.dataset.jugador === '';
  }

  asignaJugadorInicial() {
    const nRandom = Math.floor(Math.random() * 2) + 1;
    this.jugadorActual = nRandom;
  }

  actualizaInfo() {
    this.turno.textContent = this.fichas[this.jugadorActual];
  }

  getJugadorCasilla(fila, columna) {
    const claseCoordenadas = `c${fila}x${columna}`;
    return document.querySelector(`.${claseCoordenadas}`).dataset.jugador;
  }

  comparaCasillas(fila1, columna1, fila2, columna2) {
    if (this.getJugadorCasilla(fila1, columna1) === '' || this.getJugadorCasilla(fila2, columna2) === '') {
      return false;
    }
    return this.getJugadorCasilla(fila1, columna1) === this.getJugadorCasilla(fila2, columna2);
  }

  check3EnRaya() {
    return (
      (this.comparaCasillas(1, 1, 1, 2) && this.comparaCasillas(1, 1, 1, 3))
      || (this.comparaCasillas(2, 1, 2, 2) && this.comparaCasillas(2, 1, 2, 3))
      || (this.comparaCasillas(3, 1, 3, 2) && this.comparaCasillas(3, 1, 3, 3))
      || (this.comparaCasillas(1, 1, 2, 1) && this.comparaCasillas(1, 1, 3, 1))
      || (this.comparaCasillas(1, 2, 2, 2) && this.comparaCasillas(1, 2, 3, 2))
      || (this.comparaCasillas(1, 3, 2, 3) && this.comparaCasillas(1, 3, 3, 3))
      || (this.comparaCasillas(1, 1, 2, 2) && this.comparaCasillas(1, 1, 3, 3))
      || (this.comparaCasillas(3, 1, 2, 2) && this.comparaCasillas(3, 1, 1, 3))
    );
  }
}

let app = new App();
